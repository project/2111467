
-- SUMMARY --

The IdRelay integration lists module gives you a block that you can place
anywhere on your site to let your users to subscribe to your newsletters.

For a full description of the module, visit the project page:
  http://drupal.org/project/idrelay_integration

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/idrelay_integration


-- REQUIREMENTS --

  Block module.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --
  Add your webservice address for getting recipients, username and password
  in the configuration settings for idRelay integration lists module.

-- CUSTOMIZATION --

  You can style the block with your own css


-- TROUBLESHOOTING --

   If the third party server is offline, your users won't be able to subscribe,
   you will get a message (form_error) in your page

-- FAQ --

Q: Do I need to have a subscription with IdRelay in order to make this work?

A: Yes.
